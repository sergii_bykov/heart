package com.deveducation.www;

public class HeartFigures {
    private static final char HEART = '❤';
    private static final char SPACE = ' ';
    private char[][] figure;

    public void drawFigure1(){
        figure = new char[][]{
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART},
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART},
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART},
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART},
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART},
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART},
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART}
        };
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawFigure2(){
        figure = new char[][]{
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART},
                {HEART, SPACE, SPACE, SPACE, SPACE, SPACE, HEART},
                {HEART, SPACE, SPACE, SPACE, SPACE, SPACE, HEART},
                {HEART, SPACE, SPACE, SPACE, SPACE, SPACE, HEART},
                {HEART, SPACE, SPACE, SPACE, SPACE, SPACE, HEART},
                {HEART, SPACE, SPACE, SPACE, SPACE, SPACE, HEART},
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART}
        };
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawFigure3(){
        figure = new char[][]{
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART},
                {HEART, SPACE, SPACE, SPACE, SPACE, HEART, SPACE},
                {HEART, SPACE, SPACE, SPACE, HEART, SPACE, SPACE},
                {HEART, SPACE, SPACE, HEART, SPACE, SPACE, SPACE},
                {HEART, SPACE, HEART, SPACE, SPACE, SPACE, SPACE},
                {HEART, HEART, SPACE, SPACE, SPACE, SPACE, SPACE},
                {HEART, SPACE, SPACE, SPACE, SPACE, SPACE, SPACE}
        };
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawFigure4(){
        figure = new char[][]{
                {HEART, SPACE, SPACE, SPACE, SPACE, SPACE, SPACE},
                {HEART, HEART, SPACE, SPACE, SPACE, SPACE, SPACE},
                {HEART, SPACE, HEART, SPACE, SPACE, SPACE, SPACE},
                {HEART, SPACE, SPACE, HEART, SPACE, SPACE, SPACE},
                {HEART, SPACE, SPACE, SPACE, HEART, SPACE, SPACE},
                {HEART, SPACE, SPACE, SPACE, SPACE, HEART, SPACE},
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART}
        };
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawFigure5(){
        figure = new char[][]{
                {SPACE, SPACE, SPACE, SPACE, SPACE, SPACE, HEART},
                {SPACE, SPACE, SPACE, SPACE, SPACE, HEART, HEART},
                {SPACE, SPACE, SPACE, SPACE, HEART, SPACE, HEART},
                {SPACE, SPACE, SPACE, HEART, SPACE, SPACE, HEART},
                {SPACE, SPACE, HEART, SPACE, SPACE, SPACE, HEART},
                {SPACE, HEART, SPACE, SPACE, SPACE, SPACE, HEART},
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART}
        };
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawFigure6(){
        figure = new char[][]{
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART},
                {SPACE, HEART, SPACE, SPACE, SPACE, SPACE, HEART},
                {SPACE, SPACE, HEART, SPACE, SPACE, SPACE, HEART},
                {SPACE, SPACE, SPACE, HEART, SPACE, SPACE, HEART},
                {SPACE, SPACE, SPACE, SPACE, HEART, SPACE, HEART},
                {SPACE, SPACE, SPACE, SPACE, SPACE, HEART, HEART},
                {SPACE, SPACE, SPACE, SPACE, SPACE, SPACE, HEART}
        };
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawFigure7(){
        figure = new char[][]{
                {HEART, SPACE, SPACE, SPACE, SPACE, SPACE, HEART},
                {SPACE, HEART, SPACE, SPACE, SPACE, HEART, SPACE},
                {SPACE, SPACE, HEART, SPACE, HEART, SPACE, SPACE},
                {SPACE, SPACE, SPACE, HEART, SPACE, SPACE, SPACE},
                {SPACE, SPACE, HEART, SPACE, HEART, SPACE, SPACE},
                {SPACE, HEART, SPACE, SPACE, SPACE, HEART, SPACE},
                {HEART, SPACE, SPACE, SPACE, SPACE, SPACE, HEART}
        };
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawFigure8(){
        figure = new char[][]{
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART},
                {SPACE, HEART, SPACE, SPACE, SPACE, HEART, SPACE},
                {SPACE, SPACE, HEART, SPACE, HEART, SPACE, SPACE},
                {SPACE, SPACE, SPACE, HEART, SPACE, SPACE, SPACE},
                {SPACE, SPACE, SPACE, SPACE, SPACE, SPACE, SPACE},
                {SPACE, SPACE, SPACE, SPACE, SPACE, SPACE, SPACE},
                {SPACE, SPACE, SPACE, SPACE, SPACE, SPACE, SPACE}
        };
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawFigure9(){
        figure = new char[][]{
                {SPACE, SPACE, SPACE, SPACE, SPACE, SPACE, SPACE},
                {SPACE, SPACE, SPACE, SPACE, SPACE, SPACE, SPACE},
                {SPACE, SPACE, SPACE, SPACE, SPACE, SPACE, SPACE},
                {SPACE, SPACE, SPACE, HEART, SPACE, SPACE, SPACE},
                {SPACE, SPACE, HEART, SPACE, HEART, SPACE, SPACE},
                {SPACE, HEART, SPACE, SPACE, SPACE, HEART, SPACE},
                {HEART, HEART, HEART, HEART, HEART, HEART, HEART}
        };
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }
}
